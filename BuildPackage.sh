#!/bin/bash
################################################################
#
# Build an installable package
#
# (c) Copyright 2013 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

VERSION='0.3.11'
# Get possible program options
while getopts h OPTION
do
    case ${OPTION} in
    *)  echo "Usage: $0 [-h]"
        exit
        ;;
    esac
done
shift $((OPTIND - 1))

# Build the altermime executable
rm -rf /tmp/altermime-0.3-dev
tar -C /tmp -xvzf altermime-0.3-dev.tar.gz
sed -i 's/-Werror //' /tmp/altermime-0.3-dev/Makefile
(cd /tmp/altermime-0.3-dev; make)


# At this time we can only create DEBIAN packages
if [ -f /etc/debian_version ]
then
    # Install GIT and FPM if necessary
    dpkg-query -W git &> /dev/null
    [ $? -ne 0 ] && apt-get -y install git
    dpkg-query -W ruby-dev &> /dev/null
    [ $? -ne 0 ] && apt-get -y install ruby-dev
    dpkg-query -W rubygems &> /dev/null
    [ $? -ne 0 ] && apt-get -y install rubygems
    [ -x /usr/local/bin/fpm ] || gem install fpm

    # Create the debian package
    CURDIR=$(pwd)
    trap "rm -rf /tmp/altermime" EXIT
    mkdir -p /tmp/altermime
    cp /tmp/altermime-0.3-dev/altermime /tmp/altermime/
    fpm -n altermime -v $VERSION -a all -C /tmp/altermime -m "consult@btoy1.net" \
      --after-install install.sh \
      --description "alterMIME is a small program which is used to alter your mime-encoded mailpack" \
      -f --package /tmp/altermime_${VERSION}_all.deb -t deb \
      --depends libc6 \
      --url 'https://gitlab.com/bluc/altermime' -s dir .
    RETCODE=$?

    [ -s /tmp/altermime_0.3.11_all.deb ] && cp /tmp/altermime_0.3.11_all.deb $CURDIR
else
    echo "Unsupported Linux distribution"
    RETCODE=1
fi
exit $RETCODE
      