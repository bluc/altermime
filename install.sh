#!/bin/bash
################################################################
#
# Install or update altermime, the init script, and man page
# Install the conf file if not yet present
#
# (c) Copyright 2013 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
################################################################

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

if [ $EUID -ne 0 ]
then
	echo "You must be 'root' to execute this script"
	exit 1
fi

DEBIAN=0
[ -f /etc/debian_version ] && DEBIAN=1

# Define the path for the sources
if [ -s /tmp/altermime/altermime ]
then
	# The debian package puts the sources there
	SRC_DIR='/tmp'
else
	# The standalone installation uses the sources in the current dir
	SRC_DIR='.'
fi

# Install the daemon script if necessary
INST_DAEMON=1
if [ -s /usr/sbin/altermime ]
then
	diff ${SRC_DIR}/altermime /usr/bin/altermime &> /dev/null
	INST_DAEMON=$?
fi
[ $INST_DAEMON -ne 0 ] && install -m 0755 -o root -g root ${SRC_DIR}/altermime /usr/bin/altermime

if [ -d /usr/share/doc ]
then
	# Install the documentation
	mkdir -p /usr/share/doc/altermime
	/usr/bin/altermime | grep -v 'Error' > ${SRC_DIR}/altermime.man
	install -m 644 ${SRC_DIR}/altermime.man /usr/share/doc/altermime
fi

# We are done 
exit 0
